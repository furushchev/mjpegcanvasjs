DEVEL - **r2**
 * Revert to setInterval over requestAnimationFrame [(wintermuteger)](https://github.com/wintermuteger)

2013-04-15 - **r1**
 * Initial development of mjpegcanvasjs [(rctoris)](https://github.com/rctoris/)
